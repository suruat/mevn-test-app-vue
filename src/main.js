import Vue from "vue";
import VueCookies from "vue-cookies";

import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

Vue.use(VueCookies);
VueCookies.config("7d");

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
